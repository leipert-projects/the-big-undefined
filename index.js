const fs = require("fs");
const querystring = require("querystring");

const yamlParser = require("js-yaml");

const { getStageInfo } = require("./lib/api");
const { printStage, printIndexPage } = require("./lib/rendering");

async function renderStages(allStages) {
  const stages = Object.entries(allStages)
    .map(([stageName, value]) => {
      const { label, backend = [], frontend = [], group = [] } = value;

      const stageLabel = querystring.unescape(label);

      return {
        stageName,
        stageLabel,
        stageLabelScope: stageLabel.includes("::")
          ? stageLabel.replace(/::.+$/, "::")
          : null,
        stageSlug: stageName
          .toLowerCase()
          .replace(/\W/g, "-")
          .replace(/-+/g, "-"),
        group: Array.isArray(group) ? group : [group],
        users: [...backend, ...frontend]
      };
    })
    .sort((a, b) => (a.stageName > b.stageName ? 1 : -1));

  const stageInformation = await Promise.all(stages.map(getStageInfo));

  printIndexPage({ stages });
  stageInformation.forEach(printStage);
}

// Get document, or throw exception on error
const asyncRetroStages = yamlParser.safeLoad(
  fs.readFileSync("./async-teams.yml", "utf8")
);
const localStages = yamlParser.safeLoad(fs.readFileSync("./teams.yml", "utf8"));

let allStages = { ...asyncRetroStages, ...localStages };

if (process.env.STAGE) {
  if (!allStages[process.env.STAGE]) {
    console.warn(
      `You are trying to filter for "${process.env.STAGE}", but it isn't defined.`
    );
    console.warn(
      `Please use one of the available stages: ${Object.keys(allStages).sort()}`
    );
    process.exit(1);
  }

  allStages = { [process.env.STAGE]: allStages[process.env.STAGE] };
}

renderStages(allStages)
  .then(() => process.exit(0))
  .catch(e => {
    console.log(e);
    return process.exit(1);
  });
