const { Gitlab } = require("gitlab");

/* Only consider MRs created after this date,
   this is a workaround, as unfortunately
   we do not have a mergedAfter filter for the MR APIs :crying: */
const START_DATE = new Date(2019, 4, 1);

const api = new Gitlab({
  token: process.env.GITLAB_TOKEN
});

let userIdCache = {};

/**
 * Retrieves userId for a given username
 *
 * Saves userId in cache, for easier retrieval
 *
 * @param username
 * @returns {Promise<null|*>}
 */
async function getUserId(username) {
  if (userIdCache[username]) {
    return userIdCache[username];
  }
  try {
    return (userIdCache[username] = (await api.Users.all({
      username: username
    })).find(
      user => user.username.toLowerCase() === username.toLowerCase()
    ).id);
  } catch (e) {
    console.warn(`Couldn't get userID for: [${username}]`);
    return null;
  }
}

/**
 * Get _merged_ MRs of a user since the global start date
 * @param id
 * @param stageLabel
 * @param stageLabelScope
 * @returns {Promise<*>}
 */
async function getUnlabeledMRsByAuthor({
  id,
  stageLabel,
  stageLabelScope = null
}) {
  const MRs = await api.MergeRequests.all({
    // groupId of gitlab-org
    groupId: 9970,
    state: "merged",
    authorId: id,
    createdAfter: START_DATE
  });
  return MRs.filter(({ labels }) =>
    labels.every(label => {
      /**
       * Return false if the current label is the stage label
       * or starts with the stage label scope, e.g. `group::`
       */
      return !(
        label === stageLabel ||
        (stageLabelScope && label.startsWith(stageLabelScope))
      );
    })
  );
}

/**
 * This is where the magic happens. Get all MRs for a stage and then group them by person
 * @param userIds
 * @param stageLabel
 * @param stageLabelScope
 * @returns {Promise<{}|any[]>}
 */
async function getUnlabeledMRsForStageMembers({
  userIds,
  stageLabel,
  stageLabelScope
}) {
  if (userIds.length === 0) {
    return [];
  }
  const MRs = await Promise.all(
    userIds.map(id =>
      getUnlabeledMRsByAuthor({ id, stageLabel, stageLabelScope })
    )
  );
  const MRsByPerson = MRs.flat(2)
    .sort((a, b) => {
      if (a.merged_at === b.merged_at) {
        return 0;
      }
      return a.merged_at < b.merged_at ? 1 : -1;
    })
    .reduce((all, mr) => {
      const author = mr.author;

      if (!all[author.id]) {
        all[author.id] = {
          author,
          MRs: []
        };
      }

      all[author.id].MRs.push(mr);

      return all;
    }, {});

  return Object.values(MRsByPerson).sort((a, b) => {
    if (a.author.username === b.author.username) {
      return 0;
    }
    return a.author.username > b.author.username ? 1 : -1;
  });
}

/**
 * Get all members of a group!
 *
 * @param group
 * @returns {Promise<Array|*>}
 */
async function getGroupMembers(group) {
  try {
    return await api.GroupMembers.all(group);
  } catch (e) {
    console.warn(`Couldn't get members for: [${group}]`);
    return [];
  }
}

/**
 * Join user ids and usernames retrieved from usernames and from groups
 *
 * @param users
 * @param groups
 * @returns {Promise<{userIds: any[], usernames: *[]}>}
 */
async function getUserIdsAndNames(users, groups) {
  const userIds = (await Promise.all(
    users.map(user => getUserId(user))
  )).filter(Boolean);

  const { groupIds, groupUsernames } = (await Promise.all(
    groups.map(group => getGroupMembers(group))
  ))
    .flat(2)
    .reduce(
      (all, current) => {
        all.groupIds.push(current.id);
        all.groupUsernames.push(current.username);
        return all;
      },
      { groupIds: [], groupUsernames: [] }
    );

  return {
    userIds: [...userIds, ...groupIds],
    usernames: [...users, ...groupUsernames]
  };
}

/**
 * Get Info for the stage
 * @param info
 * @returns {Promise<{unlabeledMRs: ({}|any[]), usernames: *[]}>}
 */
async function getStageInfo(info) {
  const { users, stageLabel, group, stageLabelScope } = info;

  const { userIds, usernames } = await getUserIdsAndNames(users, group);

  const unlabeledMRs = await getUnlabeledMRsForStageMembers({
    userIds,
    stageLabel,
    stageLabelScope
  });

  return {
    ...info,
    usernames,
    unlabeledMRs
  };
}

module.exports = { getStageInfo };
