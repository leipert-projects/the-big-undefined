#!/usr/bin/env bash
# Use the unofficial bash strict mode: http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail; IFS=$'\n\t'

yarn install --production --frozen-lockfile

curl --fail --location https://gitlab.com/gitlab-org/async-retrospectives/raw/master/teams.yml --output async-teams.yml
mkdir -p public/
node ./index.js
